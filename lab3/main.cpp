#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "datetime.h"

using namespace std;

DateTime* Read_file_date(std::string file_name, int& n)
{
    ifstream file(file_name);
    string line;
    n = 0;

    while (getline(file, line))
    {
        n++;
    }

    DateTime* Date = new DateTime[n];

    file.clear();
    file.seekg(0, std::ios::beg);

    for(size_t i = 0; i < n; ++i)
    {
        file >> Date[i];
    }

    file.close();
    return Date;
}

void Out() 
{
	srand(time(0));
	DateTime date, next, prev;
	int n;
	DateTime* Date = Read_file_date("date.txt", n);
	DateTime* Prev = new DateTime[n];
	date.System_time(date);

	cout << "\nSystem time: " << date << endl << "Date read from file:\n" << endl;;

	for (int i = 0; i < n; i++)
	{
		Date[i].Get_next_date(next);
		Date[i].Get_prev_date(prev);

		cout << i + 1 << '\t' << Date[i] << '\t' << next << '\t' << prev << endl;
		Prev[i] = prev;
	}

	delete[] Date;
	delete[] Prev;
}

int main()
{
	Out();

	system("pause");

    return 0;
}